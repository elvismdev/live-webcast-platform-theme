<?php ?>
<div id="fire_chat_container">
	<div id="fire_chat_messages">
		<article ng-repeat="msg in fireChat.chat">
			<strong>{{msg.name}}</strong>: <span>{{msg.msg}}</span>
		</article>
	</div>
	<form id="fire_chat_form" ng-submit="newChat()">
		<input type="hidden" ng-model="msg.name" placeholder="Name" />
		<textarea ng-model="msg.msg" placeholder="Message"></textarea>
		<input type="submit" value="chat" />
	</form>
	<script type="text/javascript">
	jQuery('#fire_chat_form > textarea').keydown(function(event) {
		if (event.keyCode == 13) {
			$('#fire_chat_form > input[type=submit]').click();
			return false;
		}
	});
	</script>
</div>
<?php ?>
