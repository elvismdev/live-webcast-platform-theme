<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Live WITNation Theme' );
define( 'CHILD_THEME_URL', 'http://live.witnation.com/' );
define( 'CHILD_THEME_VERSION', '2.1.2' );

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

//* Enqueue Google Fonts
add_action( 'wp_enqueue_scripts', 'genesis_sample_google_fonts' );
function genesis_sample_google_fonts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:300,400,700', array(), CHILD_THEME_VERSION );

}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

// Remove default genesis header
remove_action( 'genesis_header', 'genesis_header_markup_open', 5 );
remove_action( 'genesis_header', 'genesis_do_header' );
remove_action( 'genesis_header', 'genesis_header_markup_close', 15 );


// Defining webcast link to redirect after login.
$wgs_settings = get_option( 'wgs_settings' );

// Conditional logic with time to set the proper page to redirect
if ( $wgs_settings['wgs_8am_webcast_page_id'] && $wgs_settings['wgs_1pm_webcast_page_id'] && $wgs_settings['wgs_6pm_webcast_page_id'] ) {

	$localtime = current_time( 'timestamp', 1 );
	$date8am = 1456578000;	// Saturday 27th 8:00AM
	$date1pm = 1456596000;	// Saturday 27th 1:00PM
	$date6pm = 1456614000;	// Saturday 27th 6:00PM
	$dateEndDay = 1456635540;	// Saturday 27th 11:59PM (End of the day)

	if ( ( $localtime > $date8am ) && ( $localtime < $date1pm ) ) {	// Saturday 27th between 8:00AM AND 1:00PM
		$webcast_link = get_permalink( $wgs_settings['wgs_8am_webcast_page_id'] );	// 8:00AM link
		$webcast_page_ID = $wgs_settings['wgs_8am_webcast_page_id'];
	} elseif ( ( $localtime > $date1pm ) && ( $localtime < $date6pm ) ) {	// Saturday 27th between 1:00PM AND 6:00PM
		$webcast_link = get_permalink( $wgs_settings['wgs_1pm_webcast_page_id'] );	// 1:00PM link
		$webcast_page_ID = $wgs_settings['wgs_1pm_webcast_page_id'];
	} elseif ( ( $localtime > $date6pm ) && ( $localtime < $dateEndDay ) ) {	// Saturday 27th between 6:00PM AND 11:59PM (The end of the day)
		$webcast_link = get_permalink( $wgs_settings['wgs_6pm_webcast_page_id'] );	// 6:00PM link
		$webcast_page_ID = $wgs_settings['wgs_6pm_webcast_page_id'];
	} else {
		$webcast_link = get_permalink( $wgs_settings['wgs_webcast_page_id'] );	// Regular webcast page
		$webcast_page_ID = $wgs_settings['wgs_webcast_page_id'];
	}

} else {

	$webcast_link = get_permalink( $wgs_settings['wgs_webcast_page_id'] );	// Regular webcast page
	$webcast_page_ID = $wgs_settings['wgs_webcast_page_id'];
	
}



// Add our custom header
add_action( 'genesis_header', 'lwitn_header' );
function lwitn_header() {
	global $webcast_link; ?>
	<?php if ( ! is_page( 9859 ) ):  ?>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<div class="mobile-login-btn"><?php wp_loginout( $webcast_link ); ?></div>
					<a class="navbar-brand site-logo" href="<?php echo trailingslashit( home_url() ); ?>"><?php //echo get_bloginfo( 'name' ); ?></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="navbar-form navbar-right">
						<?php wp_loginout( $webcast_link ); ?>
					</div>
				</div><!--/.navbar-collapse -->
			</div>
		</nav>
	<?php endif; ?>
	<?php }

	add_filter( 'loginout', 'lwitn_output_as_bootstrap_button' );
	function lwitn_output_as_bootstrap_button($text) {
		if ( is_user_logged_in() ) {
			$selector = 'class="btn btn-success"';
			$text = str_replace('<a ', '<a '.$selector, $text);
			return $text;
		} else {
			$selector = 'class="simplemodal-login btn btn-success"';
			$text = str_replace('<a ', '<a '.$selector, $text);
			return $text;
		}
	}

// Redirect to homepage after logout
	add_action( 'wp_logout','go_home' );
	function go_home() {
		wp_redirect( home_url() );
		exit();
	}

// Output custom field with the video embed
	add_action( 'genesis_before_entry', 'lwitn_webcast_video' );
	function lwitn_webcast_video() { ?>
	<?php if ( types_render_field( 'video-code', array('raw'=>'true') ) ): ?>
		<div class="row">
			<div class="col-md-12"> 
				<div class="webcast_video_wrapper">
					<?php echo types_render_field( 'video-code', array('raw'=>'true') ); ?>
				</div>
			</div>
			<!-- Temporary disabling the chat box -->
		<!-- <div class="col-md-4">
			<div class="webcast_chat_wrapper">
				<?php //echo do_shortcode( '[ng-chatroom id="'.types_render_field( 'chat-room-id', array('raw'=>'true') ).'"]' ); ?>
			</div>
		</div> -->
	</div>
<?php endif ?>
<?php
}

// Change default password hint for the reset password page
function lwitn_filter_password_hint( $hint ) {
	$hint = 'Hint: To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).';
	return $hint;
}
add_filter( 'password_hint', 'lwitn_filter_password_hint', 10, 1 );

