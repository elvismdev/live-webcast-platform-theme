<?php
/**
* Template Name: Main First Page
* Description: Used as a page template to show the login page
*/

function lwitn_genesis() {
	lwitn_genesis_header();

	$home_footer_cached = new FragmentCache( array( 'key' => 'lwitn-genesis-footer' ) );
	if ( !$home_footer_cached->output() ) {
		lwitn_genesis_footer();
		$home_footer_cached->store();
	}
	
}

// Custom Genesis Header
function lwitn_genesis_header() {
	global $webcast_page_ID;
	$home_genesis_header_cached = new FragmentCache( array( 'key' => 'lwitn-genesis-header' ) );
	if ( !$home_genesis_header_cached->output() ) {

		do_action( 'genesis_doctype' );
		do_action( 'genesis_title' );
		do_action( 'genesis_meta' );

	wp_head(); //* we need this for plugins
	?>
</head>
<?php
genesis_markup( array(
	'html5'   => '<body %s>',
	'xhtml'   => sprintf( '<body class="%s">', implode( ' ', get_body_class() ) ),
	'context' => 'body',
	) );
do_action( 'genesis_before' );

do_action( 'genesis_before_header' );

$home_genesis_header_cached->store();
}


do_action( 'genesis_header' );

$home_banner_cached = new FragmentCache( array( 'key' => 'home-banner-'.$webcast_page_ID, 'logged_in' => is_user_logged_in() ) );
if ( !$home_banner_cached->output() ) {
	do_action( 'genesis_after_header' );
	$home_banner_cached->store();
}

}

// Custom Genesis Footer
function lwitn_genesis_footer() {

	do_action( 'genesis_before_footer' );


	do_action( 'genesis_footer' );
	do_action( 'genesis_after_footer' );

	do_action( 'genesis_after' );
	wp_footer(); //* we need this for plugins
	?>
</body>
</html>
<?php
}

// Add Spot for a slideshow or banner for promotions
add_action('genesis_after_header', 'lwitn_jumbotron');
function lwitn_jumbotron() {
	global $webcast_link; ?>
	<div class="jumbotron">
		<div class="container">
			<?php
			$is_user_logged_in = is_user_logged_in();
			$wgs_settings = get_option( 'wgs_settings' );
			if ( !$is_user_logged_in ) {
				if ( $wgs_settings['wgs_homepage_banner_login_popup'] ) {
					$bannerLink = 'class="simplemodal-login" href="'.wp_login_url( $webcast_link ).'"';
				} else {
					$bannerLink = 'href="'.$wgs_settings['wgs_webcast_landing_page'].'"';
				}
			} else {
				$bannerLink = 'href="'.$webcast_link.'"';
			}
			?>
			<a <?= $bannerLink ?>>
				<?php
				if ( !$is_user_logged_in ) {
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					}
				} else {
					echo types_render_field( 'logged-in-users-image', array( 'title' => 'Click to Watch Now!', 'alt' => 'Click to Watch Now!' ) );
				}
				?>
			</a>
		<!-- <div class="row">
			<div class="col-md-12 no-access">
				<p>Don't have access?</p>
				<a href="http://grantcardonetv.com/cart/?add-to-cart=58265">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/invest-now-btn.png">
				</a>
			</div>
		</div> -->
	</div>
</div>
<?php }

// Output columns for the other webinars
add_action('genesis_before_footer', 'lwitn_past_webcasts');
function lwitn_past_webcasts() { ?>
<div class="container">
	<div class="row">
		<?php
		$arg = array(
			'post_type' => 'past-event'
			);
		$query = new WP_Query($arg);
		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) : $query->the_post();
		$event_url = types_render_field( 'url', array('raw'=>'true') );
		?>
		<div class="col-xs-6 col-sm-6 col-md-3 past-webcast-thumb">
			<p><a target="_blank" href="<?php echo $event_url; ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a></p>
		</div>
		<?php
		endwhile;
		endif;
		wp_reset_query();
		?>
	</div>
	<hr>
</div>
<?php }

lwitn_genesis();