<?php
/**
* Template Name: Webcast Video Page
* Description: Template for page where is gonna be hosted the webcast video with comments at right side.
*/

function lwitn_genesis() {
	global $post;
	lwitn_genesis_header();

	$webcast_footer_cached = new FragmentCache( array( 'key' => 'webcast-page-footer-'.$post->ID ) );
	if ( !$webcast_footer_cached->output() ) {
		lwitn_genesis_footer();
		$webcast_footer_cached->store();
	}

}

// Custom Genesis Header
function lwitn_genesis_header() {
	global $post;
	$webcast_page_header1_cached = new FragmentCache( array( 'key' => 'webcast-page-header1-'.$post->ID ) );
	if ( !$webcast_page_header1_cached->output() ) {

		do_action( 'genesis_doctype' );
		do_action( 'genesis_title' );

		$webcast_page_header1_cached->store();
	}


	do_action( 'genesis_meta' );

	wp_head(); //* we need this for plugins


	$webcast_page_header2_cached = new FragmentCache( array( 'key' => 'webcast-page-header2-'.$post->ID ) );
	if ( !$webcast_page_header2_cached->output() ) {
		
		?>
	</head>
	<?php
	genesis_markup( array(
		'html5'   => '<body %s>',
		'xhtml'   => sprintf( '<body class="%s">', implode( ' ', get_body_class() ) ),
		'context' => 'body',
		) );
	do_action( 'genesis_before' );

	$webcast_page_header2_cached->store();
} 

do_action( 'genesis_after_header' );
}

// Custom Genesis Footer
function lwitn_genesis_footer() {
	do_action( 'genesis_before_footer' );
	// do_action( 'genesis_footer' );
	do_action( 'genesis_after_footer' );

	do_action( 'genesis_after' );
	wp_footer(); //* we need this for plugins
	?>
</body>
</html>
<?php
}

add_action('genesis_after_header', 'lwitn_webcast');
function lwitn_webcast() {
	global $post; ?>
	<?php $webcast_video_block1_cached = new FragmentCache( array( 'key' => 'webcast-video-block1-'.$post->ID ) ); ?>
	<?php if ( !$webcast_video_block1_cached->output() ): ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-9 webcast-video-block">
					<div class="row top-video-row">
						<div class="col-md-12">
							<div class="webcast-video-header">
								<a class="site-logo-animated" href="<?php echo trailingslashit( home_url() ); ?>"><?php //echo get_bloginfo( 'name' ); ?></a>
							</div>
							<div id="navbar" class="navbar-collapse collapse">
								<div class="navbar-form navbar-right">
									<?php $webcast_video_block1_cached->store(); endif; ?>

									<?php wp_loginout(); ?>

									<?php $webcast_video_block2_cached = new FragmentCache( array( 'key' => 'webcast-video-block2-'.$post->ID ) ); ?>
									<?php if ( !$webcast_video_block2_cached->output() ): ?>
									</div>
								</div><!--/.navbar-collapse -->
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<?php // if ( substr( types_render_field( 'video-code', array( 'raw' => 'true' ) ), 0, 7 ) !== '<iframe' ) : ?>
							<!--	<div class="comeback-text">
									<p>COME BACK THURSDAY FEB 18TH AT 2PM EST TO WATCH THE GREATEST SALES SECRET WITH GRANT CARDONE</p>
								</div> -->
								<?php // endif; ?>

								<?php if ( types_render_field( 'video-code', array('raw'=>'true') ) ) : ?>
									<div class="webcast_video_wrapper" <?= ( substr( types_render_field( 'video-code', array( 'raw' => 'true' ) ), 0, 7 ) === '<iframe' ) ? 'style="height: 0; padding: 40px 0; padding-bottom: 56.25%;"' : ''  ?>>
										<?= types_render_field( 'video-code', array('raw'=>'true') ) ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="row shares-and-visitors">
							<div class="col-md-12">
								<div class="share-box">
									<?php do_action( 'addthis_widget', get_permalink(), get_the_title(), array(
										'size' => '32',
										'services' => 'twitter,facebook,google_plusone_share,linkedin',
										'type' => 'custom'
										)); ?>
									</div>
									<div class="visitors-box">
										<?php $webcast_video_block2_cached->store(); endif; ?>

										<?php if (function_exists('users_online')): ?>
											<div id="useronline-count"><?php users_online(); ?></div>
										<?php endif; ?>

										<?php $webcast_video_block3_cached = new FragmentCache( array( 'key' => 'webcast-video-block3-'.$post->ID ) ); ?>
										<?php if ( !$webcast_video_block3_cached->output() ): ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 comments-right-block">
								<div class="webcast_comments_wrapper">
									<?php $webcast_video_block3_cached->store(); endif; ?>

									<?php do_action( 'genesis_after_post' ); ?>

									<?php $webcast_video_block4_cached = new FragmentCache( array( 'key' => 'webcast-video-block4-'.$post->ID ) ); ?>
									<?php if ( !$webcast_video_block4_cached->output() ): ?>
									</div>
								</div>
							</div>
						</div>
						<?php $webcast_video_block4_cached->store(); endif; ?>
						<?php }

						lwitn_genesis();